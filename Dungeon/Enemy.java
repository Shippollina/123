package Dungeon;

import java.util.Random;

public class Enemy {

    Random random = new Random();

    private int enemyHealth;
    private int enemyAttackDamage;
    private int healthPotionDropChance;
    int healthPotionHealAmount = 30;
    private int playerAttackDamage;
    private String enemyName;
    private String Attack;
    private String AttackSound;
    private int level;

    //set and get monster's name

    public String getEnemyName() {
        return enemyName;
    }

    public void setEnemyName(String enName) {
        enemyName = enName;
    }

    //set and get monster's health

    public int getEnemyHealth() {
        enemyHealth = random.nextInt((enemyHealth)) + 1 ;
        return enemyHealth;
    }

    public void setEnemyHealth(int enHealth) {
        enemyHealth = enHealth;
    }

    //set and get the monster's attack damage

    public int getEnemyAttackDamage(){
        return this.enemyAttackDamage;
    }

    public void setEnemyAttackDamage(int enAD){
        this.enemyAttackDamage = random.nextInt(enAD) + 1;
    }

    //set and get the player's attack damage

    public int getPlayerAttackDamage(){
        return this.playerAttackDamage;
    }

    public void setPlayerAttackDamage(int plAD){
        this.playerAttackDamage = random.nextInt(plAD)  + 1;
    }

    //set and get the monster's chance to drop potion

    public int getHealthPotionDropChance(){
        return healthPotionDropChance;
    }

    public void setHealthPotionDropChance(int HPchance){
        healthPotionDropChance = HPchance;
    }

    //set and get attack type
    public String getAttack(){
        return Attack;
    }
    public void setAttack(String attack){
        Attack = attack;
    }

    //set and get attack sound
    public String getAttackSound(){
        return AttackSound;
    }
    public void setAttackSound(String attackSound){
        AttackSound = attackSound;
    }

    //set and get a level
    public int getLevel(){
        return level;
    }
    public void setLevel(int lev){
        level = lev;
    }

}
