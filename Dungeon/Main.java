package Dungeon;

//Suggestions for the future:
// 1) if statement for diff music file paths
// 2) Improve Random attack damage as it some time repeats 1 in a row
// 3) add different potion abilities (e.g. protects you for damage for 1 or 2 hits)

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Description.description();
        String musicLocation = "Dungeon/Dungeon_recording2.wav";
        Description newDescription = new Description();
        newDescription.playMusic(musicLocation);

        //if we have savedGame, we should get level from it

        Scanner sc = new Scanner(System.in);
        int savedLevel = 0;
        long seconds = 0;


        System.out.println("Is this your first time playing this game?");
        System.out.println("\t1. Yes \n\t2. No");
        String input = sc.nextLine();
        while (true) {
            if (input.equals("1")) {
                System.out.println("\t# You have started a new game! #\n");
                break;
            } else if (input.equals("2")) {
                try {
                    FileReader fr = new FileReader("SavedTime.txt");
                    sc = new Scanner(fr);
                    if (sc.hasNextLine()) {
                        String termSeconds = sc.nextLine();
                        seconds = Integer.parseInt(termSeconds);
                    }
                    sc.close();
                } catch (FileNotFoundException e) {
                    System.out.println("Ooops! File can't be read"); //and here we need to start game from scratch? use EndGame?
                    e.printStackTrace();
                }
                System.out.println("Previous time your game took " + seconds + " seconds!");
                System.out.println("Do you want to open your previous game?");
                System.out.println("\t1. Yes \n\t2. No");
                sc = new Scanner(System.in);
                input = sc.nextLine();
                while (true) {
                    if (input.equals("1")) {
                        try {
                            FileReader fr = new FileReader("SavedGame.txt");
                            sc = new Scanner(fr);
                            String tempPlayerHealth = sc.nextLine();
                            //int savedPlayerHealth = Integer.parseInt(tempPlayerHealth);
                            String tempNumHealthPotions = sc.nextLine();
                            //int SavedNumHealthPotions = Integer.parseInt(tempNumHealthPotions);
                            String tempLevel = sc.nextLine();
                            savedLevel = Integer.parseInt(tempLevel);
                            sc.close();
                            System.out.println("You keep your previous level and HP");
                        } catch (FileNotFoundException e) {
                            System.out.println("Ooops! File can't be read");//what happens here? will GAME start or no?
                            e.printStackTrace();
                        }
                        break;
                    } else if (input.equals("2")) {
                        System.out.println("\t# You have started a new game! #\n");
                        break;
                    } else {
                        System.out.println("\tInvalid command! Enter 1 or 2");
                        input = sc.nextLine();
                    }
                }
                break;
            } else {
                System.out.println("\tInvalid command! Enter 1 or 2");
                input = sc.nextLine();
            }
        }

        //game starts
        System.out.println("-------------------------------------------");

        ZonedDateTime timer = ZonedDateTime.now(); // timer starts

        Game:
        while (true) {
            if (savedLevel > 0) {
                for (int i = savedLevel + 1; i <= 4; i++) {
                    if (!level(i)) {
                        i = 4;
                        seconds = timer.until(ZonedDateTime.now(), ChronoUnit.SECONDS); // timer stop
                        TheEnd.gameEnding(seconds);
                    }
                }
            } else {
                for (int i = 1; i <= 4; i++) {
                    if (!level(i)) {
                        i = 4;
                        seconds = timer.until(ZonedDateTime.now(), ChronoUnit.SECONDS); // timer stop
                        TheEnd.gameEnding(seconds);
                    }
                }
            }
            System.out.println("Do you want to continue game from scratch?");
            System.out.println("\t1. Yes \n\t2. No");
            sc = new Scanner(System.in);
            input = sc.nextLine();
            while(true){
                if(input.equals("1")){
                    System.out.println("\t# Monsters greet you! #\n");
                    savedLevel = 0;
                    continue Game;
                    //break;
                } else if(input.equals("2")){
                    System.out.println("\t# Monsters say goodbye to you! #\n");
                    seconds = timer.until(ZonedDateTime.now(), ChronoUnit.SECONDS); // timer stop
                    TheEnd.gameEnding(seconds);
                    break;
                } else System.out.println("\tInvalid command! Enter 1 or 2");
            }
            break;
        }

    }


    public static boolean level(int level){

        boolean wasTheLevelPassed = false;
        Music newMusic = new Music();
        Fight myFight = new Fight();

        ZonedDateTime timer = ZonedDateTime.now(); // timer starts

        switch (level) {
            case 1:
                String skeleton_appear = "Dungeon\\skeleton_appear_2.wav";
                Skeleton mySkeleton = new Skeleton();mySkeleton.setEnemyName("Skeleton");
                mySkeleton.setEnemyAttackDamage(15);
                mySkeleton.setEnemyHealth(25);
                mySkeleton.setHealthPotionDropChance(25);
                mySkeleton.setPlayerAttackDamage(15);
                mySkeleton.setAttack("punch");
                mySkeleton.setLevel(1);
                mySkeleton.setAttackSound("Dungeon\\punch.wav");
                newMusic.playMusic(skeleton_appear);
                if (myFight.fight(mySkeleton) == true){
                    wasTheLevelPassed = true;
                } else {
                    wasTheLevelPassed = false;
                }
                break;
            case 2:
                String zombie_appear = "Dungeon\\zombie_appear.wav";
                Zombie myZombie = new Zombie();
                myZombie.setEnemyName("Zombie");
                myZombie.setEnemyAttackDamage(25);
                myZombie.setEnemyHealth(50);
                myZombie.setHealthPotionDropChance(35);
                myZombie.setPlayerAttackDamage(25);
                myZombie.setAttack("burn");
                myZombie.setLevel(2);
                myZombie.setAttackSound("Dungeon\\burn_2.wav");
                newMusic.playMusic(zombie_appear);
                if (myFight.fight(myZombie) == true) {
                    wasTheLevelPassed = true;
                } else {
                    wasTheLevelPassed = false;
                }
                break;
            case 3:
                String warrior_appear = "Dungeon\\warrior_appear.wav";
                Warrior myWarrior = new Warrior();
                myWarrior.setEnemyName("Warrior");
                myWarrior.setEnemyAttackDamage(35);
                myWarrior.setEnemyHealth(75);
                myWarrior.setHealthPotionDropChance(45);
                myWarrior.setPlayerAttackDamage(35);
                myWarrior.setAttack("cut");
                myWarrior.setLevel(3);
                myWarrior.setAttackSound("Dungeon\\cut.wav");
                newMusic.playMusic(warrior_appear);
                if (myFight.fight(myWarrior) == true) {
                    wasTheLevelPassed = true;
                } else {
                    wasTheLevelPassed = false;
                }
                break;
            case 4:
                String assassin_appear = "Dungeon\\assasin_appear.wav";
                Assassin myAssassin = new Assassin();myAssassin.setEnemyName("Assassin");
                myAssassin.setEnemyAttackDamage(50);
                myAssassin.setEnemyHealth(100);
                myAssassin.setHealthPotionDropChance(55);
                myAssassin.setPlayerAttackDamage(50);
                myAssassin.setAttack("shoot");
                myAssassin.setLevel(4);
                myAssassin.setAttackSound("Dungeon\\shoot_2.wav");
                newMusic.playMusic(assassin_appear);
                if (myFight.fight(myAssassin) == true) {
                    wasTheLevelPassed = true;
                    System.out.println("You have won against all enemies!");
                } else {
                    wasTheLevelPassed = false;
                }
                break;
        }
        return wasTheLevelPassed;
    }

}
