package Dungeon;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.io.File;


public class Description {
    public static void playMusic(String musicLocation) {
        try {

            File musicPath = new File(musicLocation);
            AudioInputStream audioInput = AudioSystem.getAudioInputStream(musicPath);
                Clip clip = AudioSystem.getClip();
                clip.open(audioInput);
                clip.start();

            JOptionPane.showMessageDialog(null, "Click OK to start the game!");
            clip.stop();

        } catch (Exception e) {
           // System.out.println("catch exception" + e);
            JOptionPane.showMessageDialog(null, "Error");
        }
    }
    public static void description(){
        System.out.println("\t\t\t\t\t\t# Welcome to the Dungeon! #\n\n" +
                "This is an exciting game where you have to defeat the enemy before it defeats you!\n" +
                "Do not be fooled, your opponents will not rest in peace and will not hesitate to fight with all their mighty!\n" +
                "Facing your opponent you will have a chance to attack, run, or prepare for a fight by drinking a health potion.\n\n" +
                "Skeleton, Zombie, Warrior, Assassin are already waiting for a fight ... Who will win?\n");
    }
}
