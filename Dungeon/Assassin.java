package Dungeon;

public class Assassin extends Enemy{
    @Override
    public void setEnemyHealth(int AHealth) {
        super.setEnemyHealth(AHealth);
    }

    @Override
    public void setEnemyAttackDamage(int AAD) {
        super.setEnemyAttackDamage(AAD);
    }

    @Override
    public void setHealthPotionDropChance(int HPchance) {
        super.setHealthPotionDropChance(HPchance);
    }

    @Override
    public void setAttack(String attack) {
        super.setAttack(attack);
    }

    @Override
    public void setAttackSound(String attackSound) {
        super.setAttackSound(attackSound);
    }

    @Override
    public void setEnemyName(String enName) {
        super.setEnemyName(enName);
    }

    @Override
    public void setPlayerAttackDamage(int plAD) {
        super.setPlayerAttackDamage(plAD);
    }

    @Override
    public void setLevel(int lev) {
        super.setLevel(lev);
    }
}
