package Dungeon;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Fight {

    public static boolean fight(Enemy myEnemy) {

        Music newMusic = new Music();
        Random rand = new Random();

        //declare all game variables
        String drink_HP = "Dungeon\\drink_health.wav";
        String run_away = "Dungeon\\run_2.wav";
        String you_die = "Dungeon\\you_die.wav";
        String you_win = "Dungeon\\you_win.wav";
        int level = myEnemy.getLevel(); //first we get all variables specific for the level
        String enemyName = myEnemy.getEnemyName();
        int enemyHealth = myEnemy.getEnemyHealth();
        int HealthPotionDropChance = myEnemy.getHealthPotionDropChance();
        String attack = myEnemy.getAttack();
        String attackSound = myEnemy.getAttackSound();

        int playerHealth = 0; //then we get inheriting variables
        int numHealthPotions = 0;
        if (level == 1) { // on the first lever we get start value for player health and number of health potions
            playerHealth = 100;
            numHealthPotions = 2;
        } else if (level > 1 && level < 5){ // on the next level we get these variables from savedGame
            try {
                FileReader fr = new FileReader("SavedGame.txt");
                Scanner sc = new Scanner(fr);
                if (sc.hasNextLine()) {
                    String termPlayerHealth = sc.nextLine();
                    playerHealth = Integer.parseInt(termPlayerHealth);
                    String termNumHealthPotions = sc.nextLine();
                    numHealthPotions = Integer.parseInt(termNumHealthPotions);
                }
                sc.close();
            } catch (FileNotFoundException e) {
                System.out.println("Ooops! File can't be read"); //and here we need to start game from scratch? use EndGame?
                e.printStackTrace();
            }
        }

        System.out.println("\t# " + enemyName + " appeared! #\n");

        boolean running = true;
        int counter = 0; // count the number off attacks

        GAME:

        while (enemyHealth > 0 && playerHealth > 0) {
            int skattack = rand.nextInt(myEnemy.getEnemyAttackDamage()) + 1;
            int plattack = rand.nextInt(myEnemy.getPlayerAttackDamage()) + 1;
            System.out.println("\tYour HP: " + playerHealth);
            System.out.println("\tYou have " + numHealthPotions + " health potions");
            System.out.println("\t" + enemyName + "'s HP: " + enemyHealth);
            System.out.println("\n\tWhat would you like to do?");
            System.out.println("\t1. Attack");
            System.out.println("\t2. Drink health potion");
            System.out.println("\t3. Run!");

            Scanner sc2 = new Scanner(System.in);
            String userInput = sc2.nextLine();
            if (userInput.equals("1")) {
                counter++;
                newMusic.playMusic(attackSound);
                enemyHealth = enemyHealth - plattack;
                playerHealth = playerHealth - skattack;

                System.out.println("\tYou " + attack + " the " + enemyName + " for " + plattack + " damage.");
                System.out.println("\tYou receive " + skattack + " in retaliation!");

                if (playerHealth < 1) {
                    newMusic.playMusic(you_die);
                    System.out.println("\tYou have taken too much damage, you are too weak to go on!");
                    running = false;
                }

            } else if (userInput.equals("2")) {
                if (numHealthPotions > 0) {
                    newMusic.playMusic(drink_HP);
                    if (playerHealth + myEnemy.healthPotionHealAmount > 100) {
                        System.out.println("\t> You drink a health potion, healing yourself for " + (100 - playerHealth) + ".");
                        playerHealth = 100;
                        numHealthPotions--;
                        System.out.println("\n\t> You now have " + playerHealth + " HP."
                                + "\n\t> You have " + numHealthPotions + " health potions left.\n");
                    } else {
                        playerHealth += myEnemy.healthPotionHealAmount;
                        numHealthPotions--;
                        System.out.println("\t> You drink a health potion, healing yourself for " + myEnemy.healthPotionHealAmount + "."
                                + "\n\t> You now have " + playerHealth + " HP."
                                + "\n\t> You have " + numHealthPotions + " health potions left.\n");
                    }
                } else {
                    System.out.println("\t> You have no health potions left! Defeat enemies for a chance to get one!\n");
                }

            } else if (userInput.equals("3")) {
                newMusic.playMusic(run_away);
                System.out.println("\t# You run away from the " + enemyName + "! #");
                enemyHealth = myEnemy.getEnemyHealth();
                System.out.println("\t# New " + enemyName + " appeared! #\n");
                continue GAME;
            } else {
                System.out.println("\tInvalid command!");
            }

        }

        if (playerHealth > 0) {
            newMusic.playMusic(you_win);
            System.out.println("-------------------------------------------");
            System.out.println(" # " + enemyName + " was defeated in " + counter + " attempt(s)! # ");
            if (level < 4) {
                System.out.println(" # You already won against " + level + " enemies! # ");
                if (rand.nextInt(100) < HealthPotionDropChance) {
                    numHealthPotions++;
                    System.out.println(" # The " + enemyName + " dropped a health potion! # ");
                    System.out.println(" # You now have " + numHealthPotions + " health potion(s). # ");
                }
                try {
                    FileWriter myFile = new FileWriter("SavedGame.txt");
                    String toSavePlayerHealth = Integer.toString(playerHealth);
                    String toSaveNumHealthPotions = Integer.toString(numHealthPotions);
                    myFile.write(toSavePlayerHealth + "\n");
                    myFile.write(toSaveNumHealthPotions + "\n");
                    String toSaveLevel = Integer.toString(level);
                    myFile.write(toSaveLevel);
                    myFile.close();
                    System.out.println(" # You have " + playerHealth + " HP left. Use it next time! # ");
                } catch (IOException e) {
                    System.out.println(" # Ooops! Game catch an exception. # ");
                    running = false;
                }
                if (!yourChoice()) {
                    System.out.println(" # You exit the dungeon, successful from your adventures! You won against " + level + " enemies in total! # ");
                    running = false;
                }
            }
        }
        return running;
    }


    public static boolean yourChoice() {
        boolean continueGame = true;
        Scanner sc = new Scanner(System.in);
        System.out.println("-------------------------------------------");
        System.out.println("What would you like to do now?");
        System.out.println("\t1. Continue fighting");
        System.out.println("\t2. Exit the game without saving");
        System.out.println("\t3. Exit and save the game");

        String input = sc.nextLine();

        while (!input.equals("1") && !input.equals("2") && !input.equals("3")) {
            System.out.println("Invalid command!");
            input = sc.nextLine();
        }
        if (input.equals("1")) {
            System.out.println(" # You continue on your adventure! # ");
        } else if (input.equals("2")) {
            File file = new File("SavedGame.txt");
            file.delete();
            System.out.println(" # Good decision! # ");
            continueGame = false;
        } else if (input.equals("3")) {
            continueGame = false;
        }
        return continueGame;
    }

}