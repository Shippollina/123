package Dungeon;

import java.io.FileWriter;
import java.io.IOException;

public class TheEnd {

    public static void gameEnding(long seconds){
        Music newMusic = new Music();
        String game_over = "Dungeon/game_over.wav";
        newMusic.playMusic(game_over);
        //System.out.println("Do you want to play one more time?");
        try {
            FileWriter myFile = new FileWriter("SavedTime.txt");
            String toSaveTime = Long.toString(seconds);
            myFile.write(toSaveTime + "\n");
            myFile.close();
            System.out.println(" # It took you " + seconds + " seconds! # ");
        } catch (IOException e) {
            System.out.println(" # Ooops! Game catch an exception. # ");
        }

        System.out.println("#######################");
        System.out.println("# THANKS FOR PLAYING! #");
        System.out.println("#######################");
    }
}
